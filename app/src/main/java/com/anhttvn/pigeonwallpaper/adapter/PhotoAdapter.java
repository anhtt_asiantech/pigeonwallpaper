package com.anhttvn.pigeonwallpaper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.pigeonwallpaper.R;
import com.anhttvn.pigeonwallpaper.model.Wallpaper;
import com.squareup.picasso.Picasso;
import java.util.List;


public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolderWallpaper> implements View.OnClickListener {
  private Context mContext;
  private List<Wallpaper> listImage;
  private OnclickImage mOnclick;
  public PhotoAdapter(Context context, List<Wallpaper> list, OnclickImage click) {
    mContext = context;
    listImage = list;
    mOnclick = click;
  }
  @NonNull
  @Override
  public ViewHolderWallpaper onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(mContext)
            .inflate(R.layout.item_wallpaper,parent,false);
    ViewHolderWallpaper itemView = new ViewHolderWallpaper(view);
    return itemView;
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolderWallpaper holder, int position) {
    holder.name.setText(listImage.get(position).getTitle());
    if (listImage.get(position).getPath() == null || listImage.get(position).getPath().isEmpty()) {
      holder.item.setImageResource(R.drawable.ic_no_thumbnail);
    } else {
      Picasso.with(mContext).load(listImage.get(position).getPath())
              .placeholder(R.drawable.ic_no_thumbnail)
              .error(R.drawable.ic_no_thumbnail)
              .into(holder.item);
    }
    holder.item.setTag(position);
    holder.item.setOnClickListener(this);
  }

  @Override
  public int getItemCount() {
    return listImage.size();
  }

  @Override
  public void onClick(View v) {
    int position = Integer.parseInt(v.getTag()+"");
    switch (v.getId()){
      case R.id.item:
        mOnclick.selectedPosition(position);
        break;

    }
  }


  public class ViewHolderWallpaper extends RecyclerView.ViewHolder {
    private ImageView item;
    private TextView name;
    public ViewHolderWallpaper(View view) {
      super(view);
      item = view.findViewById(R.id.item);
      name = view.findViewById(R.id.name);
    }
  }

  public interface OnclickImage {
    void selectedPosition(int position);
  }
}
