package com.anhttvn.pigeonwallpaper;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;

import com.anhttvn.pigeonwallpaper.databinding.ActivityMainBinding;
import com.anhttvn.pigeonwallpaper.layout.PrivatePolice;
import com.anhttvn.pigeonwallpaper.ui.download.DownloadFragment;
import com.anhttvn.pigeonwallpaper.ui.favorite.FavoriteFragment;
import com.anhttvn.pigeonwallpaper.ui.home.HomeFragment;
import com.anhttvn.pigeonwallpaper.ui.recent.RecentFragment;
import com.anhttvn.pigeonwallpaper.util.BaseActivity;
import com.anhttvn.pigeonwallpaper.util.Config;
import com.google.android.material.navigation.NavigationView;

/**
 * @author anhtt61
 * @version 1.1.1
 * @2022
 */

public class Main extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

  private ActivityMainBinding mainBinding;


  @Override
  public void init() {
    setSupportActionBar(mainBinding.header.toolbar);
    mainBinding.navView.setNavigationItemSelectedListener(this);

    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mainBinding.drawerLayout, mainBinding.header.toolbar,
            R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    mainBinding.drawerLayout.addDrawerListener(toggle);
    toggle.syncState();
    mainBinding.header.toolbar.setTitle(getString(R.string.home));
    if (savedInstanceState == null) {
      getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
              new HomeFragment()).commit();
      mainBinding.navView.setCheckedItem(R.id.home);

    }

  }

  @Override
  public View contentView() {
    mainBinding = ActivityMainBinding.inflate(getLayoutInflater());
    return mainBinding.getRoot();
  }



  @Override
  public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    int id = item.getItemId();
    switch (item.getItemId()) {
      case R.id.home:
        mainBinding.header.toolbar.setTitle(getString(R.string.home));
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                new HomeFragment()).commit();
        break;
      case R.id.recent:
        mainBinding.header.toolbar.setTitle(getString(R.string.recent));
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                new RecentFragment()).commit();
        break;
      case R.id.favorite:
        mainBinding.header.toolbar.setTitle(getString(R.string.favorite));
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                new FavoriteFragment()).commit();
        break;
      case R.id.gallery:
        mainBinding.header.toolbar.setTitle(getString(R.string.download));
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                new DownloadFragment()).commit();
        break;

      case R.id.share:
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_app));
        i.putExtra(Intent.EXTRA_TEXT, Config.URL_APP);
        startActivity(Intent.createChooser(i, "Share"));
        break;
      case R.id.rate:
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(Config.URL_APP));
        startActivity(browserIntent);
        break;
      case R.id.privacyPolice:
        Intent intent = new Intent(this, PrivatePolice.class);
        startActivity(intent);
        break;
      default:
        break;
    }
    mainBinding.drawerLayout.closeDrawer(GravityCompat.START);
    return true;
  }


  @Override
  public void onBackPressed() {
    showExit();
  }

  private void showExit() {
    AlertDialog.Builder dialogBuilder =	new AlertDialog.Builder(this);
    LayoutInflater inflater	= this.getLayoutInflater();
    View dialogView	= inflater.inflate(R.layout.layout_exits, null);
    isBannerADS(dialogView.findViewById(R.id.ads));
    dialogBuilder.setView(dialogView);
    AlertDialog b = dialogBuilder.create();
    dialogView.findViewById(R.id.btnNo).setOnClickListener(v -> {
      b.dismiss();
    });
    dialogView.findViewById(R.id.btnYes).setOnClickListener(v -> {
      finish();
    });
    b.show();
  }
  @Override
  protected void onDestroy() {
    super.onDestroy();

  }
}