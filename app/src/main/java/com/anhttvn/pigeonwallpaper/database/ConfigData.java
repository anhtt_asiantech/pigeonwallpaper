package com.anhttvn.pigeonwallpaper.database;

public class ConfigData {
  public static final String DATABASE = "RabbitWallpaper.db";
  public static final int VERSION = 1;
  public static final String TABLE = "RabbitWallpaper";
  public static final String TB_NEW_WALLPAPER ="NewWallpaper";
  public static final String TABLE_FAVORITE = "TABLE_FAVORITE";

  // detail table
  public static final String TITLE = "title";
  public static final String PATH = "path";
  public static final String FAVORITE ="Favorite";

  // create table
  public static final String CREATE_TABLE =
          "CREATE TABLE " + TABLE + " (" +
                  TITLE + " VARCHAR(1000)," +
                  PATH +" VARCHAR(1000))";


  // create table
  public static final String CREATE_TABLE_FAVORITE =
          "CREATE TABLE " + TABLE_FAVORITE + " (" +
                  TITLE + " VARCHAR(1000)," +
                  PATH +" VARCHAR(1000))";
  /**
   * table new wallpaper
   */
  public static final String ID ="id";

  // create table
  public static final String CREATE_TABLE_NEWS =
          "CREATE TABLE " + TB_NEW_WALLPAPER + " (" +
                  ID + " VARCHAR(1000) PRIMARY KEY," +
                  TITLE + " VARCHAR(1000)," +
                  PATH +" VARCHAR(1000))";

  //delete table
  public static final String DELETE_TABLE =
          "DROP TABLE IF EXISTS " + TABLE;
  public static final String DELETE_TB_NEW_WALLPAPER =
          "DROP TABLE IF EXISTS " + TB_NEW_WALLPAPER;
  public static final String DELETE_TB_FAVORITE_WALLPAPER =
          "DROP TABLE IF EXISTS " + TABLE_FAVORITE;
}
