package com.anhttvn.pigeonwallpaper.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.anhttvn.pigeonwallpaper.model.Wallpaper;

import java.util.ArrayList;

public class DatabaseHandler extends SQLiteOpenHelper {
    public DatabaseHandler(Context context) {
      super(context, ConfigData.DATABASE, null, ConfigData.VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
      db.execSQL(ConfigData.CREATE_TABLE);
      db.execSQL(ConfigData.CREATE_TABLE_NEWS);
      db.execSQL(ConfigData.CREATE_TABLE_FAVORITE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
      db.execSQL(ConfigData.DELETE_TABLE);
      db.execSQL(ConfigData.DELETE_TB_NEW_WALLPAPER);
      db.execSQL(ConfigData.DELETE_TB_FAVORITE_WALLPAPER);
      onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
      onUpgrade(db, oldVersion, newVersion);
    }

    public void updateWallpaper(Wallpaper wallpaper ) {
      SQLiteDatabase db = this.getWritableDatabase();
      ContentValues values = new ContentValues();
      values.put(ConfigData.TITLE,wallpaper.getTitle());
      values.put(ConfigData.PATH,wallpaper.getPath());
      db.insert(ConfigData.TABLE, null,values);
      db.close();
    }

  public void updateFavoriteWallpaper(Wallpaper wallpaper ) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = new ContentValues();
    values.put(ConfigData.TITLE,wallpaper.getTitle());
    values.put(ConfigData.PATH,wallpaper.getPath());
    db.insert(ConfigData.TABLE_FAVORITE, null,values);
    db.close();
  }

    public void updateNewWallpapers(Wallpaper wallpaper) {
      SQLiteDatabase db = this.getWritableDatabase();
      ContentValues values = new ContentValues();
      values.put(ConfigData.ID, wallpaper.getId());
      values.put(ConfigData.TITLE,wallpaper.getTitle());
      values.put(ConfigData.PATH,wallpaper.getPath());
      db.insert(ConfigData.TB_NEW_WALLPAPER, null,values);
      db.close();
    }


    public boolean isWallpaper(String title) {
      SQLiteDatabase db = this.getReadableDatabase();
      Cursor cursor = db.query(ConfigData.TABLE, null,
              ConfigData.TITLE + " = ?", new String[] { String.valueOf(title) },
              null, null, null);
      cursor.moveToFirst();
      int count = cursor.getCount();
      cursor.close();
      return count > 0;
    }

  public boolean isFavoriteWallpaper(String title) {
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.query(ConfigData.TABLE_FAVORITE, null,
            ConfigData.TITLE + " = ?", new String[] { String.valueOf(title) },
            null, null, null);
    cursor.moveToFirst();
    int count = cursor.getCount();
    cursor.close();
    return count > 0;
  }

    public boolean isNewsWallpaper(String id) {
      SQLiteDatabase db = this.getReadableDatabase();
      Cursor cursor = db.query(ConfigData.TB_NEW_WALLPAPER, null,
              ConfigData.ID + " = ?", new String[] { String.valueOf(id) },
              null, null, null);
      cursor.moveToFirst();
      int count = cursor.getCount();
      cursor.close();
      return count > 0;
    }
    public boolean deleteFavoriteWallpaper(String title) {
      SQLiteDatabase db = this.getReadableDatabase();
      return db.delete(ConfigData.TABLE_FAVORITE,ConfigData.TITLE +" =?",
              new String[] {String.valueOf(title)}) > 0;
    }
    public ArrayList<Wallpaper> listWallpaper (String table) {
      ArrayList<Wallpaper> list = new ArrayList<>();
      String query = "SELECT * FROM " + table;
      SQLiteDatabase db = this.getReadableDatabase();
      Cursor cursor = db.rawQuery(query,null);
      cursor.moveToFirst();
      while (!cursor.isAfterLast()) {
        Wallpaper grammar = new Wallpaper();
        grammar.setTitle(cursor.getString(0));
        grammar.setPath(cursor.getString(1));
        list.add(grammar);
        cursor.moveToNext();
      }
      cursor.close();
      return list;
    }

  public ArrayList<Wallpaper> listNewWallpaper () {
    ArrayList<Wallpaper> list = new ArrayList<>();
    String query = "SELECT * FROM " + ConfigData.TB_NEW_WALLPAPER;
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.rawQuery(query,null);
    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
      Wallpaper grammar = new Wallpaper();
      grammar.setId(cursor.getString(0));
      grammar.setTitle(cursor.getString(1));
      grammar.setPath(cursor.getString(2));
      list.add(grammar);
      cursor.moveToNext();
    }
    cursor.close();
    return list;
  }
}