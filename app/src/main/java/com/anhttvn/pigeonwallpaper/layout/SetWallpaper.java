package com.anhttvn.pigeonwallpaper.layout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.anhttvn.pigeonwallpaper.R;
import com.anhttvn.pigeonwallpaper.databinding.ActivitySetWallpaperBinding;
import com.anhttvn.pigeonwallpaper.model.Wallpaper;
import com.anhttvn.pigeonwallpaper.util.BaseActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * @author anhtt61
 * @version 1.1.1
 * @2022
 */
public class SetWallpaper extends BaseActivity {
  private ActivitySetWallpaperBinding setWallpaperBinding;
  private Wallpaper wallpaper;
  private boolean isAllVisibleFab = false;

  @Override
  public void init() {
    getSupportActionBar().hide();
    setWallpaperBinding.fab.fabScreenLock.setVisibility(View.GONE);
    setWallpaperBinding.fab.fabScreenHome.setVisibility(View.GONE);
    setWallpaperBinding.fab.fabHomeLock.setVisibility(View.GONE);
    setWallpaperBinding.fab.fabFavorite.setVisibility(View.GONE);

    initView(false);
    isBannerADS(setWallpaperBinding.ads);
    getDataIntent();
    eventFab();
    changeIconFavorite(db.isFavoriteWallpaper(wallpaper.getTitle()));
  }

  private void initView(boolean isVisible) {
    setWallpaperBinding.fab.titleLock.setVisibility(!isVisible ? View.GONE : View.VISIBLE);
    setWallpaperBinding.fab.titleHome.setVisibility(!isVisible ? View.GONE : View.VISIBLE);
    setWallpaperBinding.fab.homeLock.setVisibility(!isVisible ? View.GONE : View.VISIBLE);
    setWallpaperBinding.fab.titleFavorite.setVisibility(!isVisible ? View.GONE : View.VISIBLE);
  }

  private void eventFab() {
    setWallpaperBinding.fab.fabAdd.setOnClickListener(v -> {
      if (!isAllVisibleFab) {
        setWallpaperBinding.fab.fabScreenLock.show();
        setWallpaperBinding.fab.fabScreenHome.show();
        setWallpaperBinding.fab.fabHomeLock.show();
        setWallpaperBinding.fab.fabFavorite.show();
        initView(true);
        isAllVisibleFab = true;
      } else {

        setWallpaperBinding.fab.fabScreenLock.hide();
        setWallpaperBinding.fab.fabScreenHome.hide();
        setWallpaperBinding.fab.fabHomeLock.hide();
        setWallpaperBinding.fab.fabFavorite.hide();
        initView(false);
        isAllVisibleFab = false;
      }
    });

    setWallpaperBinding.fab.fabScreenLock.setOnClickListener(v -> {
      if (wallpaper == null || wallpaper.getPath() == null) {
        return;
      }
      ProgressDialog dialog = new ProgressDialog(this);
      dialog.setMessage(getString(R.string.please_set_wallpaper));
      dialog.show();
      Picasso.with(this)
              .load(wallpaper.getPath())
              .into(new Target() {
                @Override
                public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from){
                  lockWallpaper(bitmap);
                  new android.os.Handler().postDelayed(
                          new Runnable() {
                            public void run() {
                              Intent intent = new Intent(Intent.ACTION_MAIN);
                              intent.addCategory(Intent.CATEGORY_HOME);
                              intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                              startActivity(intent);
                              dialog.dismiss();
                              finish();
                            }
                          },
                          1000);

                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                  dialog.dismiss();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
              });
    });

    setWallpaperBinding.fab.fabScreenHome.setOnClickListener(v -> {
      if (wallpaper == null || wallpaper.getPath() == null) {
        return;
      }
      ProgressDialog dialog = new ProgressDialog(this);
      dialog.setMessage(getString(R.string.please_set_wallpaper));
      dialog.show();
      Picasso.with(this)
              .load(wallpaper.getPath())
              .into(new Target() {
                @Override
                public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from){
                  homeWallpaper(bitmap);
                  new android.os.Handler().postDelayed(
                          new Runnable() {
                            public void run() {
                              Intent intent = new Intent(Intent.ACTION_MAIN);
                              intent.addCategory(Intent.CATEGORY_HOME);
                              intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                              startActivity(intent);
                              dialog.dismiss();
                              finish();
                            }
                          },
                          1000);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                  dialog.dismiss();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
              });
    });

    setWallpaperBinding.fab.fabHomeLock.setOnClickListener(v -> {
      if (wallpaper == null || wallpaper.getPath() == null) {
        return;
      }
      ProgressDialog dialog = new ProgressDialog(this);
      dialog.setMessage(getString(R.string.please_set_wallpaper));
      dialog.show();
      Picasso.with(this)
              .load(wallpaper.getPath())
              .into(new Target() {
                @Override
                public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from){
                  homeWallpaper(bitmap);
                  lockWallpaper(bitmap);
                  new android.os.Handler().postDelayed(
                          new Runnable() {
                            public void run() {
                              Intent intent = new Intent(Intent.ACTION_MAIN);
                              intent.addCategory(Intent.CATEGORY_HOME);
                              intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                              startActivity(intent);
                              dialog.dismiss();
                              finish();
                            }
                          },
                          1000);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                  dialog.dismiss();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
              });

    });

    // download
    setWallpaperBinding.download.setOnClickListener(v -> {
      if (wallpaper == null || wallpaper.getPath() == null) {
        return;
      }
      downloadImage(wallpaper.getPath(), wallpaper.getTitle());
    });

    setWallpaperBinding.fab.fabFavorite.setOnClickListener(v -> {
      changeFavorite();
    });
  }

  private void changeIconFavorite(boolean is) {
    setWallpaperBinding.fab.fabFavorite.setImageResource(is ? R.drawable.icon_favorite : R.drawable.icon_favorite_disable);
  }

  private void changeFavorite() {
    boolean isExits = db.isFavoriteWallpaper(wallpaper.getTitle());
    if (isExits) {
      changeIconFavorite(false);
      db.deleteFavoriteWallpaper(wallpaper.getTitle());
    } else {
      changeIconFavorite(true);
      db.updateFavoriteWallpaper(wallpaper);
    }
  }


  @Override
  public View contentView() {
    setWallpaperBinding = ActivitySetWallpaperBinding.inflate(getLayoutInflater());
    return setWallpaperBinding.getRoot();
  }

  private void getDataIntent() {
    Bundle bundle = getIntent().getExtras();
    if (bundle == null) {
      return;
    }
    wallpaper = (Wallpaper) bundle.getSerializable("wallpaper");
    if (wallpaper != null) {
      setWallpaperBinding.title.setText(wallpaper.getTitle());
      Picasso.with(getApplicationContext()).load(wallpaper.getPath())
              .placeholder(R.drawable.ic_no_thumbnail)
              .error(R.drawable.ic_no_thumbnail)
              .into(setWallpaperBinding.imgWallpaper);
    }

  }


  public void onBack(View view) {
    isADSFull();
    finish();
  }

  @Override
  public void onBackPressed() {
    isADSFull();
    super.onBackPressed();
  }
}
