package com.anhttvn.pigeonwallpaper.layout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.anhttvn.pigeonwallpaper.R;
import com.anhttvn.pigeonwallpaper.databinding.ActivityGralleryWallpaperBinding;
import com.anhttvn.pigeonwallpaper.util.BaseActivity;

import java.io.File;

public class SettingWallpaper extends BaseActivity {
  private ActivityGralleryWallpaperBinding gralleryWallpaperBinding;
  private boolean isAllVisibleFab = false;
  private String path;
  private Bitmap bitmap;

  @Override
  public void init() {
    getSupportActionBar().hide();
    gralleryWallpaperBinding.fab.setWallpaper.setVisibility(View.GONE);
    gralleryWallpaperBinding.fab.fabScreenHome.setVisibility(View.GONE);
    isFab(false);
    isBannerADS(gralleryWallpaperBinding.ads);
    loadData();
    eventFab();
  }

  @Override
  public View contentView() {
    gralleryWallpaperBinding = ActivityGralleryWallpaperBinding.inflate(getLayoutInflater());
    return gralleryWallpaperBinding.getRoot();
  }

  private void loadData() {
    Bundle bundle = getIntent().getExtras();
    if (bundle == null) {
      return;
    }
    gralleryWallpaperBinding.title.setText(R.string.app_name);
    path = (String)bundle.getSerializable("wallpaper");
    if (path == null || path.isEmpty()) {
      return;
    }
    File imgFile = new File(path);
    if(imgFile.exists())
    {
      bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
      gralleryWallpaperBinding.imgWallpaper.setImageBitmap(bitmap);
    }
  }

  private void isFab(boolean isVisible) {
    gralleryWallpaperBinding.fab.titleLock.setVisibility(!isVisible ? View.GONE : View.VISIBLE);
    gralleryWallpaperBinding.fab.titleHome.setVisibility(!isVisible ? View.GONE : View.VISIBLE);
    gralleryWallpaperBinding.fab.setWallpaper.setVisibility(!isVisible ? View.GONE : View.VISIBLE);
    gralleryWallpaperBinding.fab.fabScreenHome.setVisibility(!isVisible ? View.GONE : View.VISIBLE);
  }

  private void eventFab() {
    gralleryWallpaperBinding.fab.fabAdd.setOnClickListener(v -> {
      if (!isAllVisibleFab) {
        gralleryWallpaperBinding.fab.setWallpaper.show();
        gralleryWallpaperBinding.fab.fabScreenHome.show();
        isFab(true);
        isAllVisibleFab = true;
      } else {

        gralleryWallpaperBinding.fab.setWallpaper.hide();
        gralleryWallpaperBinding.fab.fabScreenHome.hide();
        isFab(false);
        isAllVisibleFab = false;
      }
    });

    gralleryWallpaperBinding.fab.setWallpaper.setOnClickListener(v -> {
      if (bitmap == null) {
        return;
      }
      ProgressDialog dialog = new ProgressDialog(this);
      dialog.setMessage(getString(R.string.please_set_wallpaper));
      dialog.show();
      homeWallpaper(bitmap);
      new android.os.Handler().postDelayed(
              () -> {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                dialog.dismiss();
                finish();
              },
              1000);


    });

    gralleryWallpaperBinding.fab.fabScreenHome.setOnClickListener(v -> {
      if (path == null || path.isEmpty()) {
        return;
      }
      Intent sharingIntent = new Intent(Intent.ACTION_SEND);
      Uri screenshotUri = Uri.parse(path);

      sharingIntent.setType("image/jpeg");
      sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
      startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_image)));
    });

  }

  public void onBack(View view) {
    finish();
  }
}
