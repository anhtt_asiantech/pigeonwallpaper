package com.anhttvn.pigeonwallpaper.ui.recent;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.pigeonwallpaper.adapter.PhotoAdapter;
import com.anhttvn.pigeonwallpaper.database.ConfigData;
import com.anhttvn.pigeonwallpaper.databinding.FragmentRecentBinding;
import com.anhttvn.pigeonwallpaper.layout.SetWallpaper;
import com.anhttvn.pigeonwallpaper.model.Wallpaper;
import com.anhttvn.pigeonwallpaper.util.BaseFragment;
import com.anhttvn.pigeonwallpaper.util.Config;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @author  anhtt61
 * @version 1.1.1
 * @2022
 */
public class RecentFragment extends BaseFragment implements PhotoAdapter.OnclickImage {
  private FragmentRecentBinding recentBinding;
  private PhotoAdapter mPhotoAdapter;
  private List<Wallpaper> listImage = new ArrayList<>();
  private DatabaseReference mDatabase;

  @Override
  protected View initView(LayoutInflater inflater, ViewGroup container, boolean b) {
    recentBinding = FragmentRecentBinding.inflate(inflater, container, b);
    return recentBinding.getRoot();
  }

  @Override
  protected void init() {
    mDatabase = FirebaseDatabase.getInstance().getReference();
    recentBinding.progressBar.setVisibility(View.VISIBLE);
    recentBinding.listRecent.setVisibility(View.GONE);
    recentBinding.noData.getRoot().setVisibility(View.GONE);
    if (!isConnected()) {
      recentBinding.noData.getRoot().setVisibility(View.VISIBLE);
      recentBinding.progressBar.setVisibility(View.GONE);
      recentBinding.listRecent.setVisibility(View.GONE);
      listImage = db.listWallpaper(ConfigData.TABLE);
      adapter(listImage);
    } else {
      listImage = loadWallpapers();
    }
  }

  private List<Wallpaper> loadWallpapers()  {
    List<Wallpaper> list = new ArrayList<>();
    DatabaseReference ref2;
    ref2 = mDatabase.child(Config.WALLPAPER);

    ref2.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        for (DataSnapshot dsp : dataSnapshot.getChildren()) {
          Wallpaper wallpaper = dsp.getValue(Wallpaper.class);
          if (!db.isWallpaper(wallpaper.getTitle())) {
            db.updateWallpaper(wallpaper);
          }
          list.add(wallpaper);
        }
        adapter(list);
        recentBinding.progressBar.setVisibility(View.GONE);
      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {
        recentBinding.progressBar.setVisibility(View.GONE);
        recentBinding.listRecent.setVisibility(View.GONE);
        recentBinding.noData.getRoot().setVisibility(View.VISIBLE);
      }

    });

    return list;
  }

  private void adapter(List<Wallpaper> wallpapers) {
    if (wallpapers != null && wallpapers.size() > 0) {
      recentBinding.listRecent.setVisibility(View.VISIBLE);
      recentBinding.noData.getRoot().setVisibility(View.GONE);
      mPhotoAdapter = new PhotoAdapter(getActivity(), listImage, this);
      RecyclerView.LayoutManager layoutManager =
              new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
      recentBinding.listRecent.setLayoutManager(layoutManager);
      recentBinding.listRecent.setItemAnimator(new DefaultItemAnimator());
      recentBinding.listRecent.setAdapter(mPhotoAdapter);
      mPhotoAdapter.notifyDataSetChanged();
    } else {
      recentBinding.listRecent.setVisibility(View.GONE);
      recentBinding.noData.getRoot().setVisibility(View.VISIBLE);
    }
  }

  @Override
  public void selectedPosition(int position) {
    Intent intent = new Intent(getActivity(), SetWallpaper.class);
    intent.putExtra("wallpaper", listImage.get(position));
    startActivity(intent);
  }
}
