package com.anhttvn.pigeonwallpaper.ui.home;

import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

import com.anhttvn.pigeonwallpaper.R;
import com.anhttvn.pigeonwallpaper.adapter.HomeAdapter;
import com.anhttvn.pigeonwallpaper.databinding.FragmentHomeBinding;
import com.anhttvn.pigeonwallpaper.layout.SetWallpaper;
import com.anhttvn.pigeonwallpaper.model.Wallpaper;
import com.anhttvn.pigeonwallpaper.util.BaseFragment;
import com.anhttvn.pigeonwallpaper.util.Config;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
/**
 * @author anhtt61
 * @version 1.1.1
 * @2022
 */

public class HomeFragment extends BaseFragment implements HomeAdapter.SlideNewsClick{

  private FragmentHomeBinding homeBinding;
  private List<Wallpaper> listImage = new ArrayList<>();
  private DatabaseReference mDatabase;

  private TextView[] dot;
  private HomeAdapter homeAdapter;
  @Override
  protected View initView(LayoutInflater inflater, ViewGroup container, boolean b) {
    homeBinding = FragmentHomeBinding.inflate(inflater, container, b);
    return homeBinding.getRoot();
  }

  @Override
  protected void init() {
    mDatabase = FirebaseDatabase.getInstance().getReference();
    homeBinding.progress.setVisibility(View.VISIBLE);
    homeBinding.noData.getRoot().setVisibility(View.GONE);
    homeBinding.wallpapers.setVisibility(View.GONE);
    showAdsBanner(homeBinding.homeAds);
    if (isConnected()) {
      loadData();
    } else {
      listImage = db.listNewWallpaper();
      if (listImage.size() > 0) {
        adapter(listImage);
      }
    }

  }

  private void adapter(List<Wallpaper> wallpapers) {
    homeBinding.progress.setVisibility(View.GONE);
    if (wallpapers != null && wallpapers.size() > 0) {
      listImage = wallpapers;
      homeBinding.wallpapers.setVisibility(View.VISIBLE);
      homeBinding.noData.getRoot().setVisibility(View.GONE);
      homeAdapter = new HomeAdapter(getActivity(), wallpapers, this);
      homeBinding.wallpapers.setAdapter(homeAdapter);
      homeBinding.wallpapers.setPageMargin(20);
      indicator(wallpapers,0);
      eventScrollPager(wallpapers);
    } else {
      homeBinding.wallpapers.setVisibility(View.GONE);
      homeBinding.noData.getRoot().setVisibility(View.VISIBLE);
    }
  }

  private void eventScrollPager(List<Wallpaper> wallpapers) {
    homeBinding.wallpapers.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

      }

      @Override
      public void onPageSelected(int position) {
        indicator(wallpapers, position);
      }

      @Override
      public void onPageScrollStateChanged(int state) {

      }
    });
  }


  private List<Wallpaper> loadData()  {
    List<Wallpaper> list = new ArrayList<>();
    DatabaseReference ref2 =  mDatabase.child(Config.NEW_WALLPAPER);
    ref2.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        for (DataSnapshot dsp : dataSnapshot.getChildren()) {
          Wallpaper wallpaper = dsp.getValue(Wallpaper.class);
          if (!db.isNewsWallpaper(wallpaper.getId())) {
            db.updateNewWallpapers(wallpaper);
          }
          list.add(wallpaper);
        }
        adapter(list);

      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {
        listImage = db.listNewWallpaper();
        if (listImage.size() > 0) {
          adapter(listImage);
        }
      }
    });

    return list;
  }

  private void indicator(List<Wallpaper> wallpapers, int position) {
    if (wallpapers.size() < 1) {
      return;
    }
    dot = new TextView[wallpapers.size()];
    homeBinding.dot.removeAllViews();
    for(int i = 0; i < wallpapers.size() ; i++) {
      dot[i] = new TextView(getActivity());
      dot[i].setText(Html.fromHtml("&#9673;"));
      dot[i].setTextSize(10);
      dot[i].setTextColor(getResources().getColor(R.color.darker_gray));
      homeBinding.dot.addView(dot[i]);
    }
    dot[position].setTextColor(getResources().getColor(R.color.red));
  }

  @Override
  public void detailNews(int position) {
    Intent intent = new Intent(getActivity(), SetWallpaper.class);
    intent.putExtra("wallpaper", listImage.get(position));
    startActivity(intent);
  }

}